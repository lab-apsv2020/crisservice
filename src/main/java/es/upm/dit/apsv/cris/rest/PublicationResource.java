package es.upm.dit.apsv.cris.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import es.upm.dit.apsv.cris.dao.PublicationDAOImplementation;
import es.upm.dit.apsv.cris.model.Publication;

@Path("/Publications")
public class PublicationResource {
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Publication> readAll() {
		return PublicationDAOImplementation.getInstance().readAll();
	}
	/*
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Publication> readAllPublications(@PathParam("id") String id){
		return PublicationDAOImplementation.getInstance().readAllPublications(id);
	}*/
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response create(Publication pnew) throws URISyntaxException {
		Publication p = PublicationDAOImplementation.getInstance().create(pnew);
		URI uri = new URI("/CRISSERVICE/rest/Publications/" + p.getId());
		return Response.created(uri).build();
	}
	
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response read(@PathParam("id") String id) {
		Publication p = PublicationDAOImplementation.getInstance().read(id);
		if (p == null) 
			return Response.status(Response.Status.NOT_FOUND).build();
		return Response.ok(p, MediaType.APPLICATION_JSON).build();
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response update(@PathParam("id") String id, Publication p) {
		Publication pold = PublicationDAOImplementation.getInstance().read(id);
		if ((pold == null) || (!pold.getId().contentEquals(p.getId())))
			return Response.notModified().build();
		PublicationDAOImplementation.getInstance().update(p);
		return Response.ok().build();
	}
	
	@DELETE
	@Path("{id}")
	public Response delete(@PathParam("id") String id) {
		Publication pold = PublicationDAOImplementation.getInstance().read(id);
		if (pold == null)
			return Response.notModified().build();
		PublicationDAOImplementation.getInstance().delete(pold);
		return Response.ok().build();
	}
}
