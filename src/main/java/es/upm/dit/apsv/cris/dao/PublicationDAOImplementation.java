package es.upm.dit.apsv.cris.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;

import es.upm.dit.apsv.cris.model.Publication;

public class PublicationDAOImplementation implements PublicationDAO {
	
	private static PublicationDAOImplementation instance = null;

	private PublicationDAOImplementation() {}
	public static PublicationDAOImplementation getInstance() {
		if(null == instance) 
			instance = new PublicationDAOImplementation();
		return instance;
	}

	@Override
	public Publication create(Publication publication) {
		Session session = SessionFactoryService.get().openSession();
		try {
			session.beginTransaction();
			session.save(publication);
			session.getTransaction().commit();
		} catch (Exception e) {
		} finally {
			session.close();
		}
		return publication;
	}

	@Override
	public Publication read(String publicationId) {
		Session session = SessionFactoryService.get().openSession();
		try {
			session.beginTransaction();
			Publication publication = session.get(Publication.class, publicationId);
			session.getTransaction().commit();
			return publication;
		} catch (Exception e) {
			return null;
		} finally {
			session.close();
		}
	}

	@Override
	public Publication update(Publication publication) {
		Session session = SessionFactoryService.get().openSession();
		try {
			session.beginTransaction();
			session.saveOrUpdate(publication);
			session.getTransaction().commit();
		} catch (Exception e) {
			
		} finally {
			session.close();
		}
		return publication;
	}

	@Override
	public Publication delete(Publication publication) {
		Session session = SessionFactoryService.get().openSession();
		try {
			session.beginTransaction();
			session.delete(publication);
			session.getTransaction().commit();
		} catch (Exception e) {
			
		} finally {
			session.close();
		}
		return publication;
	}

	@Override
	public List<Publication> readAll() {
		Session session = SessionFactoryService.get().openSession();
		List<Publication> publications = null;
		try {
			session.beginTransaction();
			publications = session.createQuery("from Publication").list();
			session.getTransaction().commit();
		} catch (Exception e) {
			return null;
		} finally {
			session.close();
		}
		return publications;
	}

	@Override
	public List<Publication> readAllPublications(String researcherId) {
		List<Publication> lista = new ArrayList<Publication>();
		for(Publication p : this.readAll())
			if (p.getAuthors() != null && p.getAuthors().indexOf(researcherId) > -1)
				lista.add(p);
		return lista;
	}

}
