package es.upm.dit.apsv.cris.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import es.upm.dit.apsv.cris.dao.PublicationDAO;
import es.upm.dit.apsv.cris.dao.PublicationDAOImplementation;
import es.upm.dit.apsv.cris.dao.ResearcherDAO;
import es.upm.dit.apsv.cris.dao.ResearcherDAOImplementation;
import es.upm.dit.apsv.cris.dataset.CSV2DB;
import es.upm.dit.apsv.cris.model.Publication;
import es.upm.dit.apsv.cris.model.Researcher;

class TestPublicationDAO {
	private Publication p;
	private PublicationDAO pdao;
	
	private Researcher r;
	private ResearcherDAO rdao;

	@BeforeAll
	static void dbSetUp() throws Exception {
		CSV2DB.loadPublicationsFromCSV("publications.csv");
		CSV2DB.loadResearchersFromCSV("researchers.csv");
	}

	@BeforeEach
	void setUp() throws Exception {
		pdao = PublicationDAOImplementation.getInstance();
		p = new Publication();
		p.setTitle("Stable crack growth in ceramics at ambient and elevated temperatures");
		p.setId("0027627463");
		p.setPublicationName("\"Journal of Engineering Materials and Technology");
		p.setPublicationDate(" Transactions of the ASME\"");
		p.setAuthors("7201639528");
		p.setCiteCount(0);
		
		rdao = ResearcherDAOImplementation.getInstance();
		r = rdao.read("13404965900");
	}

	@Test
	void testCreate() {
		pdao.delete(p);
		pdao.create(p);
		assertEquals(p, pdao.read(p.getId()));
	}

	@Test
	void testRead() {
		assertEquals(p, pdao.read(p.getId()));
	}

	@Test
	void testUpdate() {
		String oldid = p.getId();
		p.setId("2");
		pdao.update(p);
		assertEquals(p, pdao.read(p.getId()));
		p.setId(oldid);
		pdao.update(p);
	}

	@Test
	void testDelete() {
		pdao.delete(p);
		assertNull(pdao.read(p.getId()));
		pdao.create(p);
	}

	@Test
	void testReadAll() {
		assertTrue(pdao.readAll().size() > 75);
	}
	
	@Test
	void testReadAllPublications() {
		assertTrue(pdao.readAllPublications(r.getId()).size() > 0);
	}
}
